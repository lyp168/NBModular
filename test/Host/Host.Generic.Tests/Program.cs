﻿using System;
using System.Threading.Tasks;
using NBModular.Lib.Host.Generic;

namespace Host.Generic.Tests
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new HostBuilder().Run<Startup>(args);
        }
    }
}
