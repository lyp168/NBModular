﻿using System;
using Microsoft.Extensions.DependencyInjection;
using NBModular.Lib.Utils.Core;

namespace Utils.Core.Tests
{
    public abstract class BaseTest
    {
        protected readonly IServiceProvider SP;

        protected BaseTest()
        {
            var service = new ServiceCollection();
            service.AddNBModularServices();
            SP = service.BuildServiceProvider();
        }
    }
}
