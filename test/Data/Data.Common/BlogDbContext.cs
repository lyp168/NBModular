﻿using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;

namespace Data.Common
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext(IDbContextOptions options) : base(options)
        {

        }
    }
}
