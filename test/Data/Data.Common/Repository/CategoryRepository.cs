﻿using Data.Common.Domain;
using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;

namespace Data.Common.Repository
{
    public class CategoryRepository : RepositoryAbstract<CategoryEntity>, ICategoryRepository
    {
        public CategoryRepository(IDbContext context) : base(context)
        {
        }
    }
}
