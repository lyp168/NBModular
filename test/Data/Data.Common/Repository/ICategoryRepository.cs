﻿using Data.Common.Domain;
using NBModular.Lib.Data.Abstractions;

namespace Data.Common.Repository
{
    public interface ICategoryRepository : IRepository<CategoryEntity>
    {
    }
}
