﻿using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Config.Abstractions;
using NBModular.Lib.Excel.Abstractions;

namespace NBModular.Lib.Excel.EPPlus
{
    public class EPPlusExcelHandler : ExcelHandlerAbstract
    {
        public EPPlusExcelHandler(ILoginInfo loginInfo, IExcelExportHandler exportHandler, ExcelConfig config, IConfigProvider configProvider) : base(loginInfo, exportHandler, config, configProvider)
        {
        }
    }
}
