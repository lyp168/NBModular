﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NBModular.Lib.WeChat.Core
{
    public static class SignatureHelper
    {
        /// <summary>
        /// 生成jsapi_ticket签名
        /// </summary>
        /// <param name="jsapi_ticket">JsApi票据</param>
        /// <param name="noncestr">生成签名的随机串</param>
        /// <param name="timestamp">生成签名的时间戳</param>
        /// <param name="url">当前请求页面的完成url，含http协议头，不含#号后的锚数据</param>
        /// <returns>返回签名字符串</returns>
        public static string BuildTicketSignature(string jsapi_ticket, string noncestr, long timestamp, string url)
        {
            string s = $"jsapi_ticket={jsapi_ticket}&noncestr={noncestr}&timestamp={timestamp}&url={url}";
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            byte[] array = SHA1.Create().ComputeHash(bytes);
            StringBuilder stringBuilder = new StringBuilder();
            byte[] array2 = array;
            foreach (byte b in array2)
            {
                stringBuilder.Append($"{b:x2}");
            }

            return stringBuilder.ToString();
        }
    }
}
