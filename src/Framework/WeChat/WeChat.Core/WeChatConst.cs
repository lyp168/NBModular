﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBModular.Lib.WeChat.Core
{
    public static class WeChatConst
    {
        /// <summary>
        /// 获取全局ACCESS_TOKEN接口地址
        /// </summary>
        public const string CGI_BIN_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token";

        /// <summary>
        /// 获取ACCESS_TOKEN和用户OPENID接口地址
        /// </summary>
        public const string OAUTH2_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";

        /// <summary>
        /// 获取用户信息接口地址
        /// </summary>
        public const string SNS_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo";

        /// <summary>
        /// 获取JS-SDK票据接口地址
        /// </summary>
        public const string TICKET_GETTICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
    }
}
