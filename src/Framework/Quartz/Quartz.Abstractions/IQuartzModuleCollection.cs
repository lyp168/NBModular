﻿using System.Collections.Generic;

namespace NBModular.Lib.Quartz.Abstractions
{
    public interface IQuartzModuleCollection : IList<QuartzModuleDescriptor>
    {
    }
}
