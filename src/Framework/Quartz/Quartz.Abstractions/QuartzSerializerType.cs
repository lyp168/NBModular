﻿using System.ComponentModel;

namespace NBModular.Lib.Quartz.Abstractions
{
    public enum QuartzSerializerType
    {
        [Description("JSON")]
        Json,
        [Description("XML")]
        Xml
    }
}
