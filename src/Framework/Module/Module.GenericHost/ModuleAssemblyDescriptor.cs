﻿using System.Reflection;
using NBModular.Lib.Module.Abstractions;

namespace NBModular.Lib.Module.GenericHost
{
    public class ModuleAssemblyDescriptor : IModuleAssemblyDescriptor
    {
        public Assembly Application { get; set; }

        public Assembly Domain { get; set; }

        public Assembly Infrastructure { get; set; }
        public Assembly Quartz { get; set; }
    }
}
