﻿using System;
using NBModular.Lib.Host.Web.Options;

namespace NBModular.Lib.Host.Web
{
    public class DefaultStartLogoProvider : IStartLogoProvider
    {
        public void Show(HostOptions options)
        {
            if (options.HideStartLogo)
                return;

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine(@" ***************************************************************************************************************");
            Console.WriteLine(@" *                                                                                                             *");
            Console.WriteLine(@" *           $$\   $$\ $$$$$$$\  $$\      $$\                 $$\           $$\                                *");
            Console.WriteLine(@" *           $$$\  $$ |$$  __$$\ $$$\    $$$ |                $$ |          $$ |                               *");
            Console.WriteLine(@" *           $$$$\ $$ |$$ |  $$ |$$$$\  $$$$ | $$$$$$\   $$$$$$$ |$$\   $$\ $$ | $$$$$$\   $$$$$$\             *");
            Console.WriteLine(@" *           $$ $$\$$ |$$$$$$$\ |$$\$$\$$ $$ |$$  __$$\ $$  __$$ |$$ |  $$ |$$ | \____$$\ $$  __$$\            *");
            Console.WriteLine(@" *           $$ \$$$$ |$$  __$$\ $$ \$$$  $$ |$$ /  $$ |$$ /  $$ |$$ |  $$ |$$ | $$$$$$$ |$$ |  \__|           *");
            Console.WriteLine(@" *           $$ |\$$$ |$$ |  $$ |$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |$$  __$$ |$$ |                 *");
            Console.WriteLine(@" *           $$ | \$$ |$$$$$$$  |$$ | \_/ $$ |\$$$$$$  |\$$$$$$$ |\$$$$$$  |$$ |\$$$$$$$ |$$ |                 *");
            Console.WriteLine(@" *           \__|  \__|\_______/ \__|     \__| \______/  \_______| \______/ \__| \_______|\__|                 *");
            Console.WriteLine(@" *                                                                                                             *");
            Console.Write(@" *                                   ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(@"启动成功，欢迎使用 NBModular ~");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"                                            *");
            Console.WriteLine(@" *                                                                                                             *");
            Console.Write(@" *                                   ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(@"接口地址：" + options.Urls);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"                                                   *");
            Console.WriteLine(@" *                                                                                                             *");
            Console.WriteLine(@" ***************************************************************************************************************");
            Console.WriteLine();
        }
    }
}
