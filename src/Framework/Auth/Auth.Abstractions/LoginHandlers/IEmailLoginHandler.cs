﻿using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions.LoginModels;

namespace NBModular.Lib.Auth.Abstractions.LoginHandlers
{
    /// <summary>
    /// 邮件登录处理
    /// </summary>
    public interface IEmailLoginHandler
    {
        /// <summary>
        /// 登录处理
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<LoginResultModel> Handle(EmailLoginModel model);
    }
}
