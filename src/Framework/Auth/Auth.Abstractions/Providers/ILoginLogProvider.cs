﻿using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions.LoginModels;

namespace NBModular.Lib.Auth.Abstractions.Providers
{
    /// <summary>
    /// 登录日志处理
    /// </summary>
    public interface ILoginLogProvider
    {
        /// <summary>
        /// 处理
        /// </summary>
        /// <param name="loginResult">登录结果</param>
        /// <returns></returns>
        Task Handle(LoginResultModel loginResult);
    }
}
