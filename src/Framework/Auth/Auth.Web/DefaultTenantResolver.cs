﻿using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Auth.Abstractions.LoginModels;
using NBModular.Lib.Utils.Core.Attributes;

namespace NBModular.Lib.Auth.Web
{
    [Singleton]
    public class DefaultTenantResolver : ITenantResolver
    {
        public Task Resolve(LoginResultModel loginResultModel)
        {
            loginResultModel.TenantId = null;
            return Task.CompletedTask;
        }
    }
}
