﻿using System;

using System.IO;

namespace Qiniu.Util
{
    /// <summary>
    /// 环境变量-用户路径
    /// </summary>
    public class UserEnv
    {


        /// <summary>
        /// 找到QHome目录（在当前目录下建立的"QHome"文件夹）
        /// </summary>
        /// <returns>QHOME路径</returns>
        public static string GetHomeFolder()
        {
            //当前路径
            string homeFolder = Path.GetFullPath("./QHome");

            if(!Directory.Exists(homeFolder))
            {
                Directory.CreateDirectory(homeFolder);
            }
            return homeFolder;
        }


    }
}
