﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NBModular.Lib.OSS.Minio.Models
{
    internal enum PresignedObjectType
    {
        Put,
        Get,
    }
}
