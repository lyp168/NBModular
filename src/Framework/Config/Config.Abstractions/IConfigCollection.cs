﻿using System.Collections.Generic;

namespace NBModular.Lib.Config.Abstractions
{
    /// <summary>
    /// 配置集合
    /// </summary>
    public interface IConfigCollection : IList<ConfigDescriptor>
    {

    }
}
