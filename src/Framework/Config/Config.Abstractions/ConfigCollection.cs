﻿using NBModular.Lib.Utils.Core.Abstracts;

namespace NBModular.Lib.Config.Abstractions
{
    public class ConfigCollection : CollectionAbstract<ConfigDescriptor>, IConfigCollection
    {
    }
}
