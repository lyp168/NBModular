﻿using NBModular.Lib.Data.Abstractions.Entities;
using NBModular.Lib.Utils.Core.Abstracts;

namespace NBModular.Lib.Data.Core.Entities
{
    /// <summary>
    /// 列信息集合
    /// </summary>
    public class ColumnDescriptorCollection : CollectionAbstract<IColumnDescriptor>, IColumnDescriptorCollection
    {
    }
}
