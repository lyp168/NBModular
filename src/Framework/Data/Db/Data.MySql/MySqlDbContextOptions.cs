﻿using System.Data;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Data.Abstractions.Options;
using NBModular.Lib.Data.Core;

namespace NBModular.Lib.Data.MySql
{
    /// <summary>
    /// MySql数据库上下文配置项
    /// </summary>
    public class MySqlDbContextOptions : DbContextOptionsAbstract
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbOptions"></param>
        /// <param name="options"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="loginInfo"></param>
        public MySqlDbContextOptions(DbOptions dbOptions, DbModuleOptions options, ILoggerFactory loggerFactory, ILoginInfo loginInfo) : base(dbOptions, options, new MySqlAdapter(dbOptions, options, loggerFactory), loggerFactory, loginInfo)
        {
        }

        public override IDbConnection NewConnection()
        {
            return new MySqlConnection(DbModuleOptions.ConnectionString);
        }
    }
}
