﻿using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Data.Abstractions.Options;
using NBModular.Lib.Data.Core;

namespace NBModular.Lib.Data.SqlServer
{
    /// <summary>
    /// 数据库上下文配置项SqlServer实现
    /// </summary>
    public class SqlServerDbContextOptions : DbContextOptionsAbstract
    {
        public SqlServerDbContextOptions(DbOptions dbOptions, DbModuleOptions options, ILoggerFactory loggerFactory, ILoginInfo loginInfo) : base(dbOptions, options, new SqlServerAdapter(dbOptions, options, loggerFactory), loggerFactory, loginInfo)
        {
        }

        public override IDbConnection NewConnection()
        {
            return new SqlConnection(DbModuleOptions.ConnectionString);
        }
    }
}