﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NBModular.Lib.Host.Web;

namespace NBModular.Module.Admin.WebHost
{
    public class Startup : StartupAbstract
    {
        public Startup(IHostEnvironment env, IConfiguration cfg) : base(env, cfg)
        {
        }
    }
}
