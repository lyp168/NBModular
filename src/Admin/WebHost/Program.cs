﻿using NBModular.Lib.Host.Web;

namespace NBModular.Module.Admin.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new HostBuilder().Run<Startup>(args);
        }
    }
}
