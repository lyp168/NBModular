﻿using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Auth.Web;
using NBModular.Lib.Utils.Core.Attributes;
using NBModular.Module.Admin.Application.AuthService;

namespace NBModular.Module.Admin.Web.Core
{
    [Singleton]
    public class SingleAccountLoginHandler : ISingleAccountLoginHandler
    {
        private readonly IAuthService _authService;
        private readonly ILoginInfo _loginInfo;

        public SingleAccountLoginHandler(IAuthService authService, ILoginInfo loginInfo)
        {
            _authService = authService;
            _loginInfo = loginInfo;
        }

        public async Task<bool> Validate()
        {
            var authInfo = await _authService.GetAuthInfo(_loginInfo.AccountId, _loginInfo.Platform);
            if (authInfo != null && authInfo.LoginTime != _loginInfo.LoginTime)
            {
                return true;
            }

            return false;
        }
    }
}
