﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Auth.Web;
using NBModular.Lib.Utils.Core.Attributes;
using NBModular.Lib.Utils.Core.Enums;
using NBModular.Module.Admin.Infrastructure.AccountPermissionResolver;

namespace NBModular.Module.Admin.Web.Core
{
    /// <summary>
    /// 权限验证
    /// </summary>
    [Singleton]
    public class PermissionValidateHandler : IPermissionValidateHandler
    {
        private readonly ILoginInfo _loginInfo;
        private readonly IAccountPermissionResolver _permissionResolver;

        public PermissionValidateHandler(ILoginInfo loginInfo, IAccountPermissionResolver permissionResolver)
        {
            _loginInfo = loginInfo;
            _permissionResolver = permissionResolver;
        }

        public async Task<bool> Validate(IDictionary<string, string> routeValues, HttpMethod httpMethod)
        {
            var permissions = await _permissionResolver.Resolve(_loginInfo.AccountId, _loginInfo.Platform);

            var area = routeValues["area"];
            var controller = routeValues["controller"];
            var action = routeValues["action"];
            return permissions.Any(m => m.EqualsIgnoreCase($"{area}_{controller}_{action}_{httpMethod}"));
        }
    }
}
