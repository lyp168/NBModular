﻿using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Domain.Tenant
{
    /// <summary>
    /// 租户仓储
    /// </summary>
    public interface ITenantRepository : IRepository<TenantEntity>
    {
    }
}
