﻿using NBModular.Lib.Config.Abstractions;
using NBModular.Lib.Data.Query;

namespace NBModular.Module.Admin.Domain.Config.Models
{
    public class ConfigQueryModel : QueryModel
    {
        public string Key { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public ConfigType? Type { get; set; }
    }
}
