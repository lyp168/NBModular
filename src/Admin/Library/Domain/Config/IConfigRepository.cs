﻿using System.Threading.Tasks;
using NBModular.Lib.Config.Abstractions;
using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Domain.Config
{
    /// <summary>
    /// 配置项仓储
    /// </summary>
    public interface IConfigRepository : IRepository<ConfigEntity>
    {
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<ConfigEntity> Get(ConfigType type, string code);
    }
}
