﻿using NBModular.Lib.Data.Query;

namespace NBModular.Module.Admin.Domain.Role.Models
{
    public class RoleQueryModel : QueryModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
