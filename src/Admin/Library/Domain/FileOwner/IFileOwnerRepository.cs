﻿using System.Threading.Tasks;
using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Domain.FileOwner
{
    public interface IFileOwnerRepository : IRepository<FileOwnerEntity>
    {
        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<bool> Exist(FileOwnerEntity entity);
    }
}
