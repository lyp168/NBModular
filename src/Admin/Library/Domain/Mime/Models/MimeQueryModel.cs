﻿using NBModular.Lib.Data.Query;

namespace NBModular.Module.Admin.Domain.Mime.Models
{
    public class MimeQueryModel : QueryModel
    {
        public string Ext { get; set; }
    }
}
