﻿using System;
using System.Collections.Generic;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Utils.Core.Validations;

namespace NBModular.Module.Admin.Application.RoleService.ViewModels
{
    public class RolePermissionBindModel
    {
        [NotEmpty(ErrorMessage = "请选择角色")]
        public Guid RoleId { get; set; }

        public Platform Platform { get; set; }

        public List<string> Permissions { get; set; }
    }
}
