﻿using AutoMapper;
using NBModular.Lib.Mapper.AutoMapper;
using NBModular.Module.Admin.Application.RoleService.ViewModels;
using NBModular.Module.Admin.Domain.Role;

namespace NBModular.Module.Admin.Application.RoleService
{
    public class MapperConfig : IMapperConfig
    {
        public void Bind(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RoleAddModel, RoleEntity>();
            cfg.AddMap<RoleEntity, RoleUpdateModel>();
        }
    }
}
