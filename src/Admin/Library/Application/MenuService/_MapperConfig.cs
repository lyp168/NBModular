﻿using AutoMapper;
using NBModular.Lib.Mapper.AutoMapper;
using NBModular.Module.Admin.Application.MenuService.ResultModels;
using NBModular.Module.Admin.Application.MenuService.ViewModels;
using NBModular.Module.Admin.Domain.Menu;

namespace NBModular.Module.Admin.Application.MenuService
{
    public class MapperConfig : IMapperConfig
    {
        public void Bind(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<MenuAddModel, MenuEntity>();
            cfg.AddMap<MenuEntity, MenuUpdateModel>();
            cfg.CreateMap<MenuEntity, MenuTreeResultModel>();
        }
    }
}
