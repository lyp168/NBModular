﻿using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Auth.Abstractions.LoginModels;
using NBModular.Lib.Auth.Abstractions.Providers;
using NBModular.Lib.Utils.Core.Attributes;
using NBModular.Module.Admin.Domain.LoginLog;

namespace NBModular.Module.Admin.Application.AuthService.Defaults
{
    [Singleton]
    internal class DefaultLoginLogHandler : ILoginLogProvider
    {
        private readonly ILoginLogRepository _repository;
        private readonly ILoginInfo _loginInfo;

        public DefaultLoginLogHandler(ILoginLogRepository repository, ILoginInfo loginInfo)
        {
            _repository = repository;
            _loginInfo = loginInfo;
        }

        public Task Handle(LoginResultModel model)
        {
            var entity = new LoginLogEntity
            {
                AccountId = model.AccountId,
                UserName = model.UserName,
                Email = model.Email,
                Error = model.Error,
                LoginMode = model.LoginMode,
                LoginTime = model.LoginTime,
                Phone = model.Phone,
                Platform = model.Platform,
                Success = model.Success,
                IP = _loginInfo.IP,
                UserAgent = _loginInfo.UserAgent
            };
            return _repository.AddAsync(entity);
        }
    }
}
