﻿using System;
using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions.LoginHandlers;
using NBModular.Lib.Auth.Abstractions.LoginModels;
using NBModular.Lib.Utils.Core.Attributes;

namespace NBModular.Module.Admin.Application.AuthService.LoginHandler
{
    /// <summary>
    /// 自定义登录默认实现
    /// </summary>
    [Singleton]
    public class DefaultCustomLoginHandler : ICustomLoginHandler
    {
        public Task<LoginResultModel> Handle(CustomLoginModel model)
        {
            throw new NotImplementedException();
        }
    }
}
