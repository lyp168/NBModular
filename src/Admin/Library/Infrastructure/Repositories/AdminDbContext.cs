﻿using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;

namespace NBModular.Module.Admin.Infrastructure.Repositories
{
    public class AdminDbContext : DbContext
    {
        public AdminDbContext(IDbContextOptions options) : base(options)
        {
        }
    }
}
