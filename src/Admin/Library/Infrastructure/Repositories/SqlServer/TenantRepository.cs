﻿using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;
using NBModular.Module.Admin.Domain.Tenant;

namespace NBModular.Module.Admin.Infrastructure.Repositories.SqlServer
{
    public class TenantRepository : RepositoryAbstract<TenantEntity>, ITenantRepository
    {
        public TenantRepository(IDbContext context) : base(context)
        {
        }
    }
}
