﻿using System.Threading.Tasks;
using NBModular.Lib.Config.Abstractions;
using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;
using NBModular.Module.Admin.Domain.Config;

namespace NBModular.Module.Admin.Infrastructure.Repositories.SqlServer
{
    public class ConfigRepository : RepositoryAbstract<ConfigEntity>, IConfigRepository
    {
        public ConfigRepository(IDbContext context) : base(context)
        {
        }

        public Task<ConfigEntity> Get(ConfigType type, string code)
        {
            return GetAsync(m => m.Type == type && m.Code == code);
        }
    }
}
