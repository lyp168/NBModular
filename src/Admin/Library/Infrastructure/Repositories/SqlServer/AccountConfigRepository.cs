﻿using System;
using System.Threading.Tasks;
using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;
using NBModular.Module.Admin.Domain.AccountConfig;

namespace NBModular.Module.Admin.Infrastructure.Repositories.SqlServer
{
    public class AccountConfigRepository : RepositoryAbstract<AccountConfigEntity>, IAccountConfigRepository
    {
        public AccountConfigRepository(IDbContext context) : base(context)
        {
        }

        public Task<AccountConfigEntity> GetByAccount(Guid accountId)
        {
            return GetAsync(m => m.AccountId == accountId);
        }
    }
}
