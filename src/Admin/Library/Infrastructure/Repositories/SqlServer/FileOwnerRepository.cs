﻿using System.Threading.Tasks;
using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;
using NBModular.Module.Admin.Domain.FileOwner;

namespace NBModular.Module.Admin.Infrastructure.Repositories.SqlServer
{
    public class FileOwnerRepository : RepositoryAbstract<FileOwnerEntity>, IFileOwnerRepository
    {
        public FileOwnerRepository(IDbContext context) : base(context)
        {
        }

        public Task<bool> Exist(FileOwnerEntity entity)
        {
            return Db.Find(m => m.SaveId == entity.SaveId && m.AccountId == entity.AccountId).ExistsAsync();
        }
    }
}
