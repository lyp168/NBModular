﻿using System;
using System.Threading.Tasks;
using NBModular.Lib.Auth.Abstractions;
using NBModular.Lib.Data.Abstractions;
using NBModular.Lib.Data.Core;
using NBModular.Module.Admin.Domain.AccountAuthInfo;

namespace NBModular.Module.Admin.Infrastructure.Repositories.SqlServer
{
    public class AccountAuthInfoRepository : RepositoryAbstract<AccountAuthInfoEntity>, IAccountAuthInfoRepository
    {
        public AccountAuthInfoRepository(IDbContext context) : base(context)
        {
        }

        public Task<AccountAuthInfoEntity> Get(Guid accountId, Platform platform)
        {
            return Db.Find(m => m.AccountId == accountId && m.Platform == platform).FirstAsync();
        }

        public Task<AccountAuthInfoEntity> GetByRefreshToken(string refreshToken)
        {
            return Db.Find(m => m.RefreshToken == refreshToken).FirstAsync();
        }
    }
}
