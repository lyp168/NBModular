﻿using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Infrastructure.Repositories.PostgreSQL
{
    public class FileRepository : SqlServer.FileRepository
    {
        public FileRepository(IDbContext context) : base(context)
        {
        }
    }
}
