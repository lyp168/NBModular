﻿using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Infrastructure.Repositories.PostgreSQL
{
    public class AccountRoleRepository : SqlServer.AccountRoleRepository
    {
        public AccountRoleRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}
