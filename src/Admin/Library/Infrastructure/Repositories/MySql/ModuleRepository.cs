﻿using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Infrastructure.Repositories.MySql
{
    public class ModuleRepository : SqlServer.ModuleRepository
    {
        public ModuleRepository(IDbContext context) : base(context)
        {
        }
    }
}
