﻿using NBModular.Lib.Data.Abstractions;

namespace NBModular.Module.Admin.Infrastructure.Repositories.MySql
{
    public class AccountAuthInfoRepository : SqlServer.AccountAuthInfoRepository
    {
        public AccountAuthInfoRepository(IDbContext context) : base(context)
        {
        }
    }
}
