﻿using System.ComponentModel;
using System.Threading.Tasks;
using NBModular.Lib.Quartz.Abstractions;
using NBModular.Module.Admin.Application.ModuleService;

namespace NBModular.Module.Admin.Quartz
{
    [Description("模块接口请求数量同步")]
    public class ModuleApiRequestCountSyncTask : TaskAbstract
    {
        private readonly IModuleService _moduleService;
        public ModuleApiRequestCountSyncTask(ITaskLogger logger, IModuleService moduleService) : base(logger)
        {
            _moduleService = moduleService;
        }

        public override async Task Execute(ITaskExecutionContext context)
        {
            await _moduleService.SyncApiRequestCount();
        }
    }
}
